import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT,director TEXT,actor TEXT,release_date TEXT,rating DOUBLE(5,2) , PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        try:
            cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        except Exception as exp1:
           print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


@app.route('/add_movie', methods=['POST'])
def add_movie():
    year=request.form['year']
    title=request.form['title']
    director=request.form['director']
    actor=request.form['actor']
    release_date=request.form['release_date']
    rating=request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql="SELECT title FROM movies WHERE UPPER(title) = UPPER(%s)"
    args=[title]
    cur.execute(sql,args)
    row=cur.fetchone()
    if row==None:
        try: 
            cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) values (%s,%s,%s,%s,%s,%s)", [year,title,director,actor,release_date,rating])
            cnx.commit()
        except Exception as exp:
            returnVal="Movie "+title+" could not be inserted: "+str(exp)
            return render_template('index.html',message=returnVal)
        returnVal="Movie "+title+" succesfully inserted"
        return render_template('index.html',message=returnVal)
    else:
        returnVal="Error: " +title+ " already exists"
        return render_template('index.html',message=returnVal)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    year=request.form['year']
    title=request.form['title']
    director=request.form['director']
    actor=request.form['actor']
    release_date=request.form['release_date']
    rating=request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql="SELECT title FROM movies WHERE UPPER(title) = UPPER(%s)"
    args=[title]
    cur.execute(sql,args)
    row=cur.fetchone()
    if row !=None:
        try:
            cur.execute("UPDATE movies SET year =(%s), director =(%s), actor =(%s), release_date =(%s), rating =(%s) WHERE UPPER(title) = UPPER(%s)", [year, director, actor, release_date, rating, title])
            cnx.commit()
        except Exception as exp:
            returnVal="Movie "+title+" could not be updated: "+str(exp)
            return render_template('index.html',message=returnVal)
        returnVal="Movie "+title+" succesfully updated"
        return render_template('index.html',message=returnVal)
    else:
        returnVal="Movie "+title+" does not exist"
        return render_template('index.html',message=returnVal)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)


    cur = cnx.cursor()
    sql="SELECT title FROM movies WHERE UPPER(title) = UPPER(%s)"
    args=[title]
    cur.execute(sql,args)
    row=cur.fetchone()
    if row !=None:
        try:
            sql="DELETE FROM movies WHERE UPPER(title) = UPPER(%s)"
            args=[title]
            cur.execute(sql,args)
            cnx.commit()
        except Exception as exp:
            returnVal="Movie "+title+" could not be deleted: "+str(exp)
            return render_template('index.html',message=returnVal)
        returnVal="Movie "+title+" succesfully deleted"
        return render_template('index.html',message=returnVal)
    else:
        returnVal="Movie "+title+" does not exist"
        return render_template('index.html',message=returnVal)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    actor = request.args.get('search_actor')
    myList=[]

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    sql="SELECT title FROM movies WHERE UPPER(actor) = UPPER(%s)"
    args=[actor]
    cur.execute(sql,args)
    row=cur.fetchone()
    if row==None:
        returnVal="No movies found for actor  "+actor
        return render_template('index.html',message=returnVal)  
    sql="SELECT title, year, actor FROM movies WHERE UPPER(actor) = UPPER(%s)"
    args=[actor]
    cur.execute(sql,args)
    rows=cur.fetchall()
    if rows==None:
        returnVal="No movies found for actor  "+actor
        return render_template('index.html',message=returnVal)
    else:     
        for row in rows:
            myList.append(str(row[0])+", "+str(row[1])+", "+str(row[2]))
        return render_template('index.html',message1=myList)


@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    myList=[]

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    sql="SELECT MAX(rating) as maxRating FROM movies"
    cur.execute(sql)
    returnRow=cur.fetchone()
    returnVal=returnRow[0]
    sql="SELECT title, year, actor, director, rating FROM movies WHERE rating=(%s)"
    cur.execute(sql, [returnVal])
    rows=cur.fetchall()
    for row in rows:
        myList.append(str(row[0])+", "+str(row[1])+", "+str(row[2])+", "+str(row[3])+", "+str(row[4]))
    return render_template('index.html',message1=myList)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    myList=[]

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    sql="SELECT MIN(rating) as maxRating FROM movies"
    cur.execute(sql)
    returnRow=cur.fetchone()
    returnVal=returnRow[0]
    sql="SELECT title, year, actor, director, rating FROM movies WHERE rating=(%s)"
    cur.execute(sql, [returnVal])
    rows=cur.fetchall() 
    for row in rows:
        myList.append(str(row[0])+", "+str(row[1])+", "+str(row[2])+", "+str(row[3])+", "+str(row[4]))
    return render_template('index.html',message1=myList)

@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    create_table()
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')